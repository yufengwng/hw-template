# hw-template

Write your homework in Markdown, without losing the power of LaTeX.

You can use the convenient syntax of Markdown to write your homework
assignments, but also inject LaTeX commands for mathematical things. Then you can
compile down to LaTeX and convert to PDF.

[pandoc][pd] is the converter used to convert between Markdown, LaTeX, and PDF.
pandoc uses a default template when converting Markdown to PDF, unless a custom
template is provided when it is invoked. The provided
`template.latex` is a modified version of `default.latex` found in
[pandoc-templates][pdt].

Notice that pandoc templates can have variables. These template variables are
customizable via the front-matter in your Markdown file. Check out `example.md`
as a starting point for your homework Markdown file.

## Requirements

You need the following installed:

* pandoc
* pdflatex
* add-on packages for LaTeX
* rake

See your distro's package manager for required packages. If you're on Arch
Linux, install the following for LaTeX:

* extra/texlive-core
* extra/texlive-latexextra

## Usage

1. Use `example.md` as a starting point.
2. Write some Markdown.
3. Run `rake` and get your PDF.

Check out the `Rakefile` for other useful commands. If you need some custom styles,
you have at least these options:

1. Provide your own customized `template.latex`.
2. Run `rake source` to generate the LaTeX source, edit that, then run `rake compile`.

## License

BSD 3-clause license, in accordance with [pandoc-templates][pdt].

[pd]: https://github.com/jgm/pandoc
[pdt]: https://github.com/jgm/pandoc-templates
