---
author: Your Name
date: Due Date
class: Class Name
title: Homework Title
---

This is a paragraph. Here is some inline \LaTeX math: $e = mc^2$. Here is a
number in scientific notation: 2.5\e{12}. We can have a $x$ and $y$ symbol.
We can also have equation blocks:

\begin{equation}
p(x) = \frac{1}{\sqrt{2\pi}\sigma} e^{-\frac{(x-\mu)^2}{2\sigma^2}}
\end{equation}

Here is another paragraph. Let's put a code block in here:

```python
def md_to_pdf(md_file):
    temp = convert_to_latex(md_file)
    return temp.pdf()
```

Here are some lists:

1. this
2. is
3. ordered

* this
* is
* unordered

And here is a table:

Name | Value | Description
-----|-------|------------
a    | 1     | item a
b    | 2     | item b
c    | 3     | item c

Enjoy!
